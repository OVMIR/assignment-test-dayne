var express = require('express');
var router = express.Router();

const { v4: uuidv4 } = require('uuid');
const axios = require("axios")
const cheerio = require('cheerio');

/* Processing Word Occurences. */
router.get('/process', function(req, res, next) {
  const jobId = uuidv4();
  const {url} = req.query;

  if(url){
    let resultsHash = {};
    axios.get(url)
  .then(function (response) {
    const { data } = response;
    const $ = cheerio.load(data);

    const text =  $.text();
    cleanText = text.replace(/<\/?[^>]+(>|$)/g, ""); // Remove any extra html elements

    const textArray = cleanText.split(" ");
    textArray.forEach((function(word){
      word = word.replace(/\s+/g, ''); // trim any extra spaces
      if(resultsHash.hasOwnProperty(word)){
        resultsHash[word] = resultsHash[word] + 1;
      }else{
        resultsHash[word] = 1;
      }
    }));

    // Some Post Processing to make it better
    const result = postProcessingText(resultsHash);

    //Send the response back
    res.status(200);
    return res.send({jobId: jobId, occurrence: result});
    })
    .catch(function (error) {

      console.log(error);
      res.status(400);
      return res.send({error : error})
    });
    

  }else{
    res.status(400);
    return res.send({error : "Invalid Parameters, Please Specify the URL"})
  }
});

module.exports = router;


const postProcessingText = (hash) => {
  let result = [];
  //Removing Numbers and Sorting Data
  for (const word in hash) {
    if(isNaN(word)){
      result.push({
        text: word,
        value: hash[word]
      });
    }
  }
  return result;
}
import './App.css';
import Home from './components/home';
import ResultsView from './components/resultsView';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import 'antd/dist/antd.css'
import 'tippy.js/dist/tippy.css';
import 'tippy.js/animations/scale.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/view/:process_id">
            <ResultsView/>
          </Route>
          <Route exact  path="/">
            <Home/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

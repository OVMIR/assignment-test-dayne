import { Typography,Result, Row, Col } from 'antd';
import Home from '../home';
import ReactWordcloud from 'react-wordcloud';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import ReactJson from 'react-json-view'


const optionsForCloud = {
    colors: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b"],
    enableTooltip: true,
    deterministic: false,
    fontFamily: "impact",
    fontSizes: [5, 60],
    fontStyle: "normal",
    fontWeight: "normal",
    padding: 1,
    rotations: 3,
    rotationAngles: [0, 90],
    scale: "sqrt",
    spiral: "archimedean",
    transitionDuration: 1000
  };

const ResultsView = (props) => {
    let location = useLocation();
    const [wordsArray, setWordsArray] = useState([]);
    const [currentURL,setCurrentURL] = useState('');
    useEffect(()=>{
        console.log(location)
        if(location && location.state && location.state.wordArray){
            setWordsArray(location.state.wordArray);
            setCurrentURL(location.state.url)
        }
    },[location]);

    return  (
        <div>
            <Row>
                <Col span={24}>
                    <Home minimal={true} url={currentURL}/>
                </Col>
            </Row>
            <Row>
                <Col span={18}>
                    <Typography.Title>
                        <Result
                            status="success"
                            title="Successfully Processed your request"
                            subTitle={<div style={{ height: 200, width: 800, margin:'auto'}}>
                                <p>Your word cloud is ready</p>
                                <ReactWordcloud options={optionsForCloud} words={wordsArray} />
                        </div>}
                        />
                    </Typography.Title>
                </Col>
                <Col span={6}>
                    <div style={{marginTop:'7rem'}}>
                        <Typography.Title level={3}>
                            Raw Data
                        </Typography.Title>
                        <ReactJson src={wordsArray}/>
                    </div>
                </Col>                
            </Row>

        </div>
    )
};

export default ResultsView;
import { useState } from 'react';
import { Input,Typography } from 'antd';
import { Row, Col } from 'antd';
import { Progress } from 'antd';
import {validURL} from '../../utility';
import { useHistory } from 'react-router';
const { Search } = Input;
const API_URL = 'http://localhost:8000';

const processingStage =(['Processing your request ....','Fetching data from the remote url ....','Getting Contents for you ....','Processing & Finalyzing your results ....'])

const Home = (props) => {
    const history = useHistory();
    const [activeStage, setActiveStage] = useState(0);
    const [loadingInProcess, setLoadingInProcess] = useState(false);
    const [isMinimal, setMinimal] = useState(props.minimal ? props.minimal : false );

    const pushToNextState = (wordOccurrenceArray,jobId,url) => {
        if(wordOccurrenceArray && jobId){
            history.push({
                pathname: `/view/${jobId}`,
                state: { wordArray: wordOccurrenceArray , url : url}
            });          
        }
    }

    const triggerProcessing = (url) => {
        if(url && validURL(url)){
            setLoadingInProcess(true);
            setTimeout(()=>{
                setActiveStage(activeStage + 1);
            },1000);
            fetch(`${API_URL}/process?url=${url}`).then(response => response.json())
            .then((data) =>{
                if(data){
                    setActiveStage(activeStage + 1);
                    setLoadingInProcess(false);
                    pushToNextState(data.occurrence, data.jobId , url)
                }
            });
        }
    }
    return (  
        <div>
            {!isMinimal && 
                <Row>
                    <Col span={24}>
                        <Typography.Title level={2} style={{marginTop:100}}>
                            DOM words occurrence tool
                        </Typography.Title>
                        <Typography.Paragraph>
                            Analyze the text and sort them by occurrence for any website
                        </Typography.Paragraph>
                    </Col>
                </Row>
            }
            <Row>
                <Col span={24} style={{backgroundColor: isMinimal ? 'darkslategrey' : null, padding: isMinimal ? 10 : null}}>
                    <Search
                    placeholder={isMinimal ? "Enter Another Site URL (Example : https://darthvader.com/cloning-new)" :"Enter Site URL (Example : https://darthvader.com/cloning)"}
                    allowClear
                    enterButton={loadingInProcess ? "Processing" : "Process"}
                    size="large"
                    onSearch={(e)=>{triggerProcessing(e)}}
                    style={{width:'50%',marginTop: isMinimal ? 10 : 100}}
                    loading={loadingInProcess}
                    />
                    {(props.url) && 
                        <Typography.Link href={props.url} target="_blank" style={{display:'block',color:'white',marginTop:10}}>
                        Word occurrence for URL {props.url}
                        </Typography.Link>
                    }
                </Col>
            </Row>
            <Row>
                {(loadingInProcess) && 
                <Col span={24}>
                    <Typography.Title level={4} style={{marginTop:25}}>
                        <Progress type="circle" percent={25 * (activeStage + 1)} width={50} status="active" style={{marginRight:10}}/>
                        {(processingStage[activeStage])}
                    </Typography.Title>
                    <Typography.Title level={4} style={{marginTop:25}}>
                        
                    </Typography.Title>
                    <Typography.Title level={4} style={{marginTop:25}}>
                        
                    </Typography.Title>
                    <Typography.Title level={4} style={{marginTop:25}}>
                        
                    </Typography.Title>
                </Col>
                }
            </Row>
        </div>
    )
  };

export default Home;
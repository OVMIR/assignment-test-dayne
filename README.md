
# assignment-test-mir

### Backend -> PORT 8000
### Frontend -> PORT 3000


## Things done : 

 - Backend Implementation, API that inputs url and generates word occurences from the content of the url, Scrape Content, Text, Post Processing.
 - Frontend Implementation: Home page & a Results view page with browsable history, Word Cloud for data visualization / Raw Data , Basic Validation for URLs etc

## Things To be : 

 - Unit Test Cases


## Loom Video (Demo):

https://www.loom.com/share/b52d6051852644aeb0caf1e2d312b68b

## Improvements: 

 1. Polling (Data processing takes time and sometimes API can take more than 60 seconds to return response, thus we don't have to rely on api response to show data, API can return id and we can poll the id for status)
 2. Queuing System to process.
 3. Remove Stop words to get better data (words)
 4. Validations on Frontend for errors / Invalid URLS etc
